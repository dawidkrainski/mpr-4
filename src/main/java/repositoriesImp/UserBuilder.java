package repositoriesImp;

import java.sql.ResultSet;
import java.sql.SQLException;

import repositories.IEntityBuilder;
import entities.User;

public class UserBuilder implements IEntityBuilder<User> {

	public User build(ResultSet rs) throws SQLException {
		User result = new User();
		result.setId(rs.getInt("id"));
		result.setLogin(rs.getString("login"));
		result.setPassword(rs.getString("password"));
		return result;
	}

}