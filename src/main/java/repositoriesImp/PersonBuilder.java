package repositoriesImp;

import java.sql.ResultSet;
import java.sql.SQLException;

import repositories.IEntityBuilder;
import entities.Person;

public class PersonBuilder implements IEntityBuilder<Person>{

	public Person build(ResultSet rs) throws SQLException {
		Person person = new Person();
		person.setFirstName(rs.getString("name"));
		person.setSurname(rs.getString("surname"));
		person.setPesel(rs.getString("pesel"));
		person.setEmail(rs.getString("email"));
		person.setNip(rs.getString("nip"));
		person.setDateOfBirth(rs.getDate("dateOfBirth"));
		person.setId(rs.getInt("id"));
		return person;
	}

}