package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;

import entities.Entity;

public interface IEntityBuilder<TEntity extends Entity> {

	public TEntity build(ResultSet rs) throws SQLException;
	
}